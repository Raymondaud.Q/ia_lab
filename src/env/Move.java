package env;

public enum Move{
	Forward,
	Backward,
	Left,
	Right;
};
