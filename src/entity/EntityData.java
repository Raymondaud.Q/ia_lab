package entity;

import java.io.Serializable;

public class EntityData implements Serializable
{

	private static final long serialVersionUID = 1L;
	public float x;
	public float y;
	public int nbChecked=0;
	public EntityData(float xx, float yy , int nb)
	{
		x = xx;
		y = yy;
		nbChecked = nb;
	}
}
