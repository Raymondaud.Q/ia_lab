package entity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;
import entity.ia.BasicIA;
import env.Map;

public abstract class Vehicle implements Chronometrable {
	
	private static final long serialVersionUID = 1L;
	protected float x,y;
	protected float maxSpeed = 0.5f;
	protected float speedX,speedY,speedFactor;
	protected BasicIA ia;
	protected Map map;
	protected boolean noInput = true;
	protected int targetGate = 0;
	protected boolean finished = false;
	protected Vector<Float> times = new Vector<Float>();
	int nbMove =0;
	int nbMoveTillCheckPoint=0;
	
	//#################################################	
	
	public boolean getNoInput(){ return noInput; }
	public void setNoInput(){	noInput = true; }
 	public int getTargetGate (){ return targetGate;	}
	public void increaseTargetGate(){ targetGate++;	}
	public void reinit(){ times = new Vector<Float>(); noInput = true; finished = false ; targetGate=nbMove=nbMoveTillCheckPoint=0; x = y = 10; speedX = speedY = 0f ;}
	public float x(){ return x; }
	public float y() { return y; }
	public float speedX() {
		return speedX;
	}
	public float speedY() {
		return speedY;
	}
	public int getNbMove() { return nbMove; }
	public int getNbMoveTillCheckPoint() { return nbMoveTillCheckPoint;}
	public void setNbMoveTillCheckPoint(int nb) { nbMoveTillCheckPoint = nb;}
	
	static public double sqr(double a) {
        return a*a;
    }
 
    static public double Distance(double x1, double y1, double x2, double y2) {
        return Math.sqrt(sqr(y2 - y1) + sqr(x2 - x1));
    } 
	
	public double correlation()
	{
		if ( targetGate < map.getNbGate())
		{
			float gX = map.getGate(targetGate).x();
			float gY = map.getGate(targetGate).y();
			float nX = (x + speedX*10)%map.sizeX();
			float nY = (y + speedY*10)%map.sizeY();
			return  Distance(gX,gY,nX,nY);
		}
		else 
			finish();
		return 1;
	}
	
	public Vehicle(Map m){
		x = y = 10;
		speedX = speedY = 0;
		speedFactor = 10;
		map = m;
	}
	
	public void addTime(float f)
	{
		times.add(f);
		if ( times.size() == map.getNbGate())
			finish();
	}
	
	public float getTime(int index) {
		if ( index >= times.size())
			return 0;
		else
			return times.get(index);
	}
	
	public int getNbTime()
	{
		return times.size();
	}
	
	public void finish() { finished = true;}
	
	public BasicIA getIA() { return ia;} 
	
	public boolean terminated()
	{
		return finished;
	}
	
	public void setIA(BasicIA a){
		ia = a;
	}
	
	
	public void forward(){
		if ( speedY > -maxSpeed )
			speedY-=0.01;
		noInput = false;
	}
	public void backward(){
		if ( speedY < maxSpeed )
			speedY+=0.01;
		noInput = false;
	}
	
	public void turnLeft(){
		if ( speedX > -maxSpeed )
			speedX-=0.01+0.01*speedX;
		noInput = false;
	}
	
	public void turnRight(){
		if ( speedX < maxSpeed )
			speedX+=0.01+0.01*speedX;
		noInput = false;
	}
	
	public void move(){
		if ( ia != null)
			ia.move();
		else
			System.out.println("IA NULL");
		nbMove+=1;
		x+=speedX;
		y+=speedY;
		
		if ( x > map.sizeX() )
			x = 0;
		else if ( y > map.sizeY() )
			y = 0 ;
		else if ( x < 0 )
			x = map.sizeX();
		else if ( y < 0 )
			y = map.sizeY();
		
		if ( noInput ){
			if ( speedX > 0)
				speedX -= speedX*0.001;
			else if ( speedX < 0)
				speedX += speedX*-0.001;
			if ( speedY > 0)
				speedY -= speedY*0.001;
			else if ( speedY < 0)
				speedY += speedY*-0.001;
		}
	}
	
	public void save(String path)
	{
		try {
			FileOutputStream f = new FileOutputStream(new File(path));
			ObjectOutputStream o = new ObjectOutputStream(f);
			o.writeObject(this);
			o.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException e) {
			System.out.println("Error initializing stream");
		}
	}
	public static Chronometrable load(String path)
	{
		Chronometrable lao = null;
		try {
			FileInputStream fi = new FileInputStream(new File(path));
			ObjectInputStream oi = new ObjectInputStream(fi);

			// Read objects
			lao = (Chronometrable) oi.readObject();
			oi.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lao;
	}
}