package entity.ia;

import java.io.Serializable;
import java.util.Vector;

import entity.Chronometrable;
import entity.EntityGenome;
import env.Map;
import env.Move;

public class GeneticLearning extends BasicIA implements Serializable {

	private static final long serialVersionUID = 1L;
	private EntityGenome genome;
	private float score=0;
	
	public float getScore() {
		if (subject.getNbTime() == 0) 
			score = 1000000;
		else{
			score = -subject.getNbTime();
			score -= 1/(subject.correlation());
			score -= 1/subject.getNbMoveTillCheckPoint();
			
			for ( int i = 0 ; i < subject.getNbTime() ; i++)
				score -= 1/subject.getTime(i);
			genome.good = subject.getNbMoveTillCheckPoint();
		}
		return score;
	}
	
	
	public static Vector<Chronometrable> mutate(Vector<Chronometrable> c){
		
		Vector<EntityGenome> v = EntityGenome.partouze(getBestGenomes(c,30),c.size());
		System.out.println("Mutation" + v.size());
		setGenomes(c,v);
		return c;
	}
	
	public static Vector<EntityGenome> getBestGenomes( Vector<Chronometrable> g , int percent)
	{
		Vector<EntityGenome> genomes = new Vector<EntityGenome>();
		Vector<Float> gScores = new Vector<Float>();
		for ( Chronometrable gl : g) {
			if ( gl.getIA() instanceof GeneticLearning ){
				((GeneticLearning) gl.getIA()).getGenome().reinit();
				gScores.add(((GeneticLearning) gl.getIA()).getScore());
				genomes.add(((GeneticLearning) gl.getIA()).getGenome());
			}
				
		}
		int max = genomes.size()*percent/100;
		System.out.println(max);
		Vector<EntityGenome> bestGenomes = new Vector<EntityGenome>();
		for (int i = 0; i < max ; i++ ){
			int minIndex = 0; //keep track of the index as you go. Assume at first
			float minVal= 100000000;
			for(int j = 0 ; j < gScores.size(); j++)
			  if(gScores.get(j) < minVal){
			    minVal = gScores.get(j);
			    minIndex = j;
			  }
			System.out.println("No " + i + " = " + minVal);
			bestGenomes.add(genomes.get(minIndex));
			gScores.remove(minIndex);
			genomes.remove(minIndex);
		}
			
		return bestGenomes;
	}
	
	public static void setGenomes( Vector<Chronometrable> g , Vector<EntityGenome> newGenome)
	{
		int i  = 0 ;
		for ( Chronometrable gl : g)
		{
			if ( gl.getIA() instanceof GeneticLearning && i < newGenome.size()) {
				((GeneticLearning) gl.getIA()).setGenome(newGenome.get(i));
				i+=1;
			}
		}
	}
	
	private void setGenome(EntityGenome entityGenome) {
		genome = entityGenome;
	}

	public EntityGenome getGenome(){
		return genome;
	}
	
	public void save( String fileName ) {
		genome.save(fileName);
	}
	
	public void load(String fileName) {
		genome = EntityGenome.load(fileName);
	}
	
	public GeneticLearning(Map m , Chronometrable s , int sizeGenome) {
		super(m,s);
		genome = new EntityGenome();
		genome.generateRandom(sizeGenome);
	}

	public GeneticLearning(Map m , Chronometrable s , String fileName) {
		super(m,s);
		genome = EntityGenome.load(fileName);
	}
	
	public String move(){
		String strAction = "";
		Move m = genome.move();
		if ( m == null && genome.notFinished())
		{
			subject.setNoInput();
			strAction += "NOTHING ";
			nbMove +=1;
		}
		else if ( genome.notFinished() )
		{
			nbMove +=1;
			switch (m)
			{
				case Forward:
					subject.forward();
					strAction += "Forward ";
					break;
					
				case Backward:
					subject.backward();
					strAction += "Backward ";
					break;
					
				case Left:
					subject.turnLeft();
					strAction += "TLeft ";
					break;
					
				case Right:
					subject.turnRight();
					strAction += "TRight ";
					break;
			}
		}
		else
			subject.finish();
			subject.setNoInput();
		
		//if ( strAction != "")
			//System.out.println(strAction);
		return strAction;		
	}

};
