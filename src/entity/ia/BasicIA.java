package entity.ia;

import java.io.Serializable;
import java.util.Vector;

import entity.Chronometrable;
import entity.EntityData;
import env.Map;

public class BasicIA implements Serializable{
	

	private static final long serialVersionUID = 1L;

	Map map;
	Chronometrable subject;
	EntityData target;
	int nbMove = 0;
	
	public BasicIA ( Map m , Chronometrable s) {
		map = m;
		subject = s;
	}
	
	public String move()
	{
		nbMove +=1;
		String strAction = "";
		Vector<EntityData> gates = map.getGatesPos();
		if ( subject.getTargetGate() < gates.size())
		{
			target = gates.get(subject.getTargetGate());
			if (subject.x() < target.x-10  ) {
				subject.turnRight();
				strAction+= "RTurn ";
			}
			if (subject.x() > target.x+10  ) {
				subject.turnLeft();
				strAction+= "LTurn ";
			}
			if ( subject.y() > target.y-10 ){
				subject.forward();
				strAction += "Forward ";
			}
			if ( subject.y() < target.y+10 ){
				subject.backward();		
				strAction+= "Backward ";
			}
			subject.setNoInput();
		}
		return strAction;
	}

}
