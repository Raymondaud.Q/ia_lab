package entity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Random;
import java.util.Vector;
import env.Move;

public class EntityGenome implements Serializable {

	private static final long serialVersionUID = 1L;

	transient public final static Move [] moves = {  
									Move.Forward,
									Move.Backward,
									Move.Left,
									Move.Right };
	public Vector<Move> genome;
	public int geneSize = 1;
	public int genReadIndex = 0;
	public int good=0;
	
	public EntityGenome(){
		genome = new Vector<Move>();
	}
	
	public void generateRandom(int size ) {
		//System.out.println("Generate " + size);
		genReadIndex = 0 ;
		genome = new Vector<Move>();
		Move m=null;
		for (int i = 0 ; i  < size ; i++ )
		{
			if ( i % geneSize == 0 ){
				 Random rd = new Random();
				 int mf = rd.nextInt(5);
				 if ( mf == 4 )
					 m = null;
				 else
					 m = moves[mf];
			}
			genome.add(m);
						
		}
		//for ( Move mo : genome)
			//System.out.println(mo);
	}
	
	public Move move() {
		if (genReadIndex < genome.size() ){
			Move m = genome.get(genReadIndex%genome.size());
			genReadIndex+=1;
			return m;
		}
		return null;
	}
	
	public boolean notFinished() {
		return genReadIndex != genome.size();
	}
	
	public void reinit() {
		genReadIndex = 0 ;
	}
	
	public static EntityGenome haveSex(EntityGenome male, EntityGenome female)
	{
		int mf;
		int fromBest;
		EntityGenome selected=female;
		EntityGenome children = new EntityGenome();
		Random rd = new Random();
		fromBest = rd.nextInt(21)+1;
		//System.out.println(fromBest +  " donc " + (fromBest% 2== 0) );
		if ( (fromBest%2== 0)) {
			for (int i = 0 ; i  < selected.genome.size(); i++ ){
				if ( i % selected.geneSize == 0 && rd.nextBoolean() ){
					mf = rd.nextInt(2)+1;
					if ( mf%2 == 0) 
						 selected = male;
					 else 
						 selected = female;		
				}
				children.genome.add(selected.genome.get(i));			
			}
		}
		else {
			for (int i = 0 ; i  < selected.genome.size(); i++ ){
				//System.out.println("GOOD = "+selected.good);
				if ( selected.good/1.5 < i && i % selected.geneSize == 0 && rd.nextBoolean() ){
					mf = rd.nextInt(selected.genome.size());
					if ( mf%2 == 0) 
						 selected = male;
					 else 
						 selected = female;
				}
				children.genome.add(selected.genome.get(i));			
			}
		}	
		//System.out.println(" NEW INDIV " + children.genome.size() );
		return children;
	}
	
	public static  Vector<EntityGenome> partouze (Vector<EntityGenome> best , int childrensNb){
		Vector<EntityGenome> childrens = new Vector<EntityGenome>();
		childrens.addAll(best);
		System.out.println("Taille de partouze " + childrens.size() + "/" + childrensNb);
		for ( int i = childrens.size()-1 ; i < childrensNb ; i ++){
			EntityGenome e = new EntityGenome();
			e.generateRandom(best.get(0).genome.size());
			childrens.add(haveSex(e,best.get(i%best.size())));
		}
		return childrens;
	}
	
	
	
	public void save(String fileName)
	{
		try {
			FileOutputStream f = new FileOutputStream(new File(fileName));
			ObjectOutputStream o = new ObjectOutputStream(f);
			o.writeObject(this);
			o.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException e) {
			System.out.println("Error initializing stream");
		}
	}
	
	public static EntityGenome load(String fileName)
	{
		EntityGenome lao = null;
		try {
			FileInputStream fi = new FileInputStream(new File(fileName));
			ObjectInputStream oi = new ObjectInputStream(fi);

			// Read objects
			lao = (EntityGenome) oi.readObject();
			oi.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lao;
	}
}
